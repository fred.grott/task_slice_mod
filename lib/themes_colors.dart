import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



final MaterialColor myLightPrimary = Colors.purple;
final MaterialColor myLightPrimaryVariant = Colors.deepPurple;
final MaterialColor myLightSecondary = Colors.cyan;
final MaterialColor myLightSecondaryVariant = Colors.teal;
final Color myLightBackground = Colors.white;
final Color myLightSurface = Colors.white;
final MaterialColor myLightError = Colors.red;
final Color myLightOnError =Colors.white;
final Color myLightOnPrimary = Colors.white;
final Color myLightOnSecondary = Colors.black;
final Color myLightOnBackground = Colors.black;
final Color myLightOnSurface = Colors.black;



final ColorScheme myLightColorScheme = ColorScheme(
    primary: myLightPrimary,
    primaryVariant: myLightPrimaryVariant,
    secondary: myLightSecondary,
    secondaryVariant: myLightSecondaryVariant,
    background: myLightBackground,
    surface: myLightSurface,
    error: myLightError,
    onError: myLightOnError,
    onPrimary: myLightOnPrimary,
    onSecondary: myLightOnSecondary,
    onBackground: myLightOnBackground,
    onSurface: myLightOnSurface,
    brightness: Brightness.light
);

final Color myDarkBackground = Colors.black;
final Color myDarkSurface = Colors.black87;
final Color myDarkOnBackground = Colors.white;
final Color myDarkOnSurface = Colors.white;
final Color myDarkOnPrimary = Colors.black;
final Color myDarkOnSecondary = Colors.black;
final Color myDarkError = Colors.white;
final MaterialColor myDarkOnError = Colors.red;


final ColorScheme myDarkColorScheme = ColorScheme(

    primary: myLightPrimary,
    primaryVariant: myLightPrimaryVariant,
    // do buttons switch to secondary on dark mode
    secondary: myLightSecondary,
    secondaryVariant: myLightSecondaryVariant,
    background: myDarkBackground,
    surface: myDarkSurface,
    onBackground: myDarkOnBackground,
    onSurface: myDarkOnSurface,
    onPrimary: myDarkOnPrimary,
    onSecondary: myDarkOnSecondary,
    error: myDarkError,
    onError: myDarkOnError,
    brightness: Brightness.dark
);

// Cupertino Is Different
CupertinoDynamicColor primaryPurple = CupertinoDynamicColor.withBrightnessAndContrast(
    color: Colors.purple,
    darkColor: Colors.purple,
    highContrastColor: Colors.purple[100],
    darkHighContrastColor: Colors.purple[700]
);

CupertinoDynamicColor barBackgroundPrimary = CupertinoDynamicColor.withBrightnessAndContrast(
    color: Colors.purple,
    darkColor: Colors.black87,
    highContrastColor: Colors.purple[100],
    darkHighContrastColor: Colors.deepPurple[200]
);


CupertinoDynamicColor textContentColor = CupertinoDynamicColor.withBrightness(
    color: Colors.black,
    darkColor: Colors.white
);

CupertinoDynamicColor iconButtonPrimaryColor = CupertinoDynamicColor.withBrightnessAndContrast(
    color: Colors.transparent,
    darkColor: Colors.transparent,
    highContrastColor: Colors.transparent,
    darkHighContrastColor: Colors.transparent
);

CupertinoDynamicColor appBarTextPrimaryColor = CupertinoDynamicColor.withBrightnessAndContrast(
    color: Colors.white,
    darkColor: Colors.purple[50],
    highContrastColor: Colors.purple[100],
    darkHighContrastColor: Colors.purple[100]
);

CupertinoDynamicColor actionTextColor = CupertinoDynamicColor.withBrightnessAndContrast(
    color: Colors.cyan,
    darkColor: Colors.cyan[300],
    highContrastColor: Colors.teal,
    darkHighContrastColor: Colors.teal[200]
);

CupertinoDynamicColor tabLabelTextStyleColor = CupertinoDynamicColor.withBrightnessAndContrast(
    color: Colors.grey[600],
    darkColor: Colors.grey[400],
    highContrastColor: Colors.grey[300],
    darkHighContrastColor: Colors.grey[50]
);

CupertinoDynamicColor myCupertinoDialogColor =const CupertinoDynamicColor.withBrightness(
  color: Color(0xCCF2F2F2),
  darkColor: Color(0xBF1E1E1E),
);

CupertinoDynamicColor myCupertinoDialogPressedColor = const CupertinoDynamicColor.withBrightness(
  color: Color(0xFFE1E1E1),
  darkColor: Color(0xFF2E2E2E),
);
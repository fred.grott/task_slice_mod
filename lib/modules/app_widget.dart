// BSD Claause 2 Copyright 2020 Fred Grott(Fredrick Allan Grott)
import 'package:catcher/core/catcher.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

import '../themes_colors.dart';
import '../themes_textstyles.dart';
import '../themes_widgets.dart';


// so my appstate and landingpage woudl be homemodule homepage stuff
// ignore: must_be_immutable
class AppWidget extends StatelessWidget {

  Brightness brightness = Brightness.light;
  @override
  Widget build(BuildContext context) {
    // hmm do I ned anootated region?
    // seems to work but eventually should move to using
    // annotedregion as I have nested widgets and diff appbars
    // per platforms
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    // this for full screen
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        systemNavigationBarIconBrightness: Brightness.light
    ));
    final materialTheme = ThemeData(
      // as of 2-7-2020 I still need to set this for appbar color
      primaryColor: Colors.purple,

      colorScheme: myLightColorScheme,
      textTheme: myMaterialTextTheme,
      buttonTheme: myButtonTheme,


    );
    final materialDarkTheme = ThemeData(
      brightness: Brightness.dark,


      colorScheme: myDarkColorScheme,
      textTheme: myMaterialTextTheme,


    );

    final cupertinoTheme =
    CupertinoThemeData(
      // work-around current CuperinoApp using defualt roboto fonts
      // and google_fonts pluginnot realy ready for beta
      // other part is defining fonts in pubspec
      // backout not ready yet
      // still cannot set fonts 1-30-2020 when using the
      // flutter_platform_widgets plugin but not usre if its that
      //plugin's falut
      //textTheme: const CupertinoTextThemeData(
      //textStyle: TextStyle(fontFamily: 'WorkSans')
      //),


        brightness: brightness, // if null will use the system theme
        primaryColor: primaryPurple,


        barBackgroundColor: barBackgroundPrimary,
        textTheme: myCupertinoTextTheme


    );

    // Example of optionally setting the platform upfront.
    //final initialPlatform = TargetPlatform.iOS;

    // If you mix material and cupertino widgets together then you cam
    // set this setting. Will mean ios darmk mode to not to work properly
    //final settings = PlatformSettingsData(iosUsesMaterialWidgets: true);

    // This theme is required since icons light/dark mode will look for it
    return Theme(
      data: brightness == Brightness.light ? materialTheme : materialDarkTheme,
      child: PlatformProvider(
        //initialPlatform: initialPlatform,
        builder: (context) =>
            PlatformApp(
              // so I can do vid recording of debug runs without the banner
              debugShowCheckedModeBanner: false,
              navigatorKey: Catcher.navigatorKey,
              localizationsDelegates: <LocalizationsDelegate<dynamic>>[
                DefaultMaterialLocalizations.delegate,
                DefaultWidgetsLocalizations.delegate,
                DefaultCupertinoLocalizations.delegate,
              ],
              title: 'Task Slice',
              android: (_) {
                return MaterialAppData(
                  theme: materialTheme,
                  darkTheme: materialDarkTheme,
                  themeMode: brightness == Brightness.light
                      ? ThemeMode.light
                      : ThemeMode.dark,
                );
              },
              ios: (_) =>
                  CupertinoAppData(
                    theme: cupertinoTheme,
                  ),
              builder: (BuildContext context, Widget widget) {
                Catcher.addDefaultErrorWidget(
                    showStacktrace: true,
                    customTitle: "Custom error title",
                    customDescription: "Custom error description");
                return widget;
              },
              //landingpage on app_bloc
            ),
      ),
    );

  }
}
import 'dart:ui';

import 'package:flutter_modular/flutter_modular.dart';

class AppBloc extends Disposable {
  @override
  void dispose() {}
  static Brightness brightness = brightness == Brightness.light
  ? Brightness.dark
      : Brightness.light;
}
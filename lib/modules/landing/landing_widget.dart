// BSD Claause 2 Copyright 2020 Fred Grott(Fredrick Allan Grott)




import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:taskslicemod/modules/home/home_widget.dart';

import '../../responsive_utils.dart';
import '../../themes_widgets.dart';

class LandingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // than I can call the proper
    // ResponsiveConfig.sizing in
    // my ui containers including correct font sizing

    ResponsiveConfig.init(context, allowFontScaling: true);

    return PlatformScaffold(

        iosContentPadding: true,
        appBar: PlatformAppBar(
        title:  PlatformText('Task Slice',),
    //backgroundColor: Colors.transparent,


    android: (_) => myMaterialAppBarData,


    // inmy litview pages need to set transparency
    ios: (_) => myCupertinoNavigationBarData,
    trailingActions: <Widget>[
    PlatformIconButton(
    padding: EdgeInsets.zero,
    iosIcon: Icon(CupertinoIcons.share),
    androidIcon: Icon(Icons.share),

    ios: (_) => myCupertinoIconButtonData,
    android: (_) => myMaterialIconButtonData,
    onPressed: () {},
    ),
    ],
    ),

        body: HomeWidget(title: 'Tasks'),
    );}

}
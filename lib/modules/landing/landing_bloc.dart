// BSD Claause 2 Copyright 2020 Fred Grott(Fredrick Allan Grott)


import 'dart:async';

import 'package:flutter_modular/flutter_modular.dart';

// just for show as we really do not have anything  to
// di inject at this point
class LandingBloc extends Disposable {


  // this will generate a false linter positive see
  // https://github.com/felangel/bloc/issues/587
  // ignore: close_sinks
  StreamController controller = StreamController<dynamic>();


  @override
  void dispose() {}

}
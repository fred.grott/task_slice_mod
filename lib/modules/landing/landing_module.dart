// BSD Claause 2 Copyright 2020 Fred Grott(Fredrick Allan Grott)

import 'package:flutter_modular/flutter_modular.dart';

import 'package:flutter/widgets.dart';

import 'landing_bloc.dart';
import 'landing_widget.dart';

// I either have to keep the split from my
// task slice main class or combine it in one widget
// this is the split try
class LandingModule extends MainModule {
  @override
  List<Bind> get binds => [
    Bind<dynamic>((i) => LandingBloc()),
  ];



  @override
  Widget get bootstrap => LandingWidget();

  @override
  // TODO: implement routers
  List<Router> get routers => null;
}
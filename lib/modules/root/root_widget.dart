// BSD Claause 2 Copyright 2020 Fred Grott(Fredrick Allan Grott)




// This the root of my app and thus the rest of
// the flutter platform widget boilerplate goes here
// namely everything below the theme retunr data in the
// flutter platform widgets example and the stattefull
// stuff goes in the root_bloc and get sinjected
import 'package:catcher/core/catcher.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:taskslicemod/modules/root/root_bloc.dart';

import '../../themes_colors.dart';
import '../../themes_textstyles.dart';
import '../../themes_widgets.dart';

// Because I am establishing routes from the beginning can
// persist the appbar
class RootWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    final dynamic rootBloc = Modular.get<RootBloc>();

    // Chrome block sets app as fullscreen
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    // this for full screen
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        systemNavigationBarIconBrightness: Brightness.light
    ));

    // Themes blocks
    final materialTheme = ThemeData(
      // as of 2-7-2020 I still need to set this for appbar color
      primaryColor: Colors.purple,
      colorScheme: myLightColorScheme,
      textTheme: myMaterialTextTheme,
      buttonTheme: myButtonTheme,
    );

    final materialDarkTheme = ThemeData(
      brightness: Brightness.dark,
      colorScheme: myDarkColorScheme,
      textTheme: myMaterialTextTheme,
    );

    final cupertinoTheme = CupertinoThemeData(

        brightness: rootBloc.brightness as Brightness,
        // if null will use the system theme
        primaryColor: primaryPurple,
        barBackgroundColor: barBackgroundPrimary,
        textTheme: myCupertinoTextTheme
    );


    // Example of optionally setting the platform upfront.
    //final initialPlatform = TargetPlatform.iOS;

    // If you mix material and cupertino widgets together then you cam
    // set this setting. Will mean ios darmk mode to not to work properly
    //final settings = PlatformSettingsData(iosUsesMaterialWidgets: true);

    // This theme is required since icons light/dark mode will look for it
    return Theme(
      data: rootBloc.brightness == Brightness.light ? materialTheme : materialDarkTheme,
      child: PlatformProvider(
        //initialPlatform: initialPlatform,
        builder: (context) => PlatformApp(
          // so I can do vid recording of debug runs without the banner
          debugShowCheckedModeBanner: false,
          navigatorKey: Catcher.navigatorKey,
          localizationsDelegates: <LocalizationsDelegate<dynamic>>[
            DefaultMaterialLocalizations.delegate,
            DefaultWidgetsLocalizations.delegate,
            DefaultCupertinoLocalizations.delegate,
          ],
          initialRoute: "/",
          onGenerateRoute: Modular.generateRoute,
          title: 'Task Slice',
          android: (_) {
            return MaterialAppData(
              theme: materialTheme,
              darkTheme: materialDarkTheme,
              themeMode: rootBloc.brightness == Brightness.light
                  ? ThemeMode.light
                  : ThemeMode.dark,
            );
          },
          ios: (_) => CupertinoAppData(
            theme: cupertinoTheme,
          ),
          builder:(BuildContext context, Widget widget) {
            Catcher.addDefaultErrorWidget(
                showStacktrace: true,
                customTitle: "Custom error title",
                customDescription: "Custom error description");
            return widget;
          },

        ),
      ),
    );
  }



}

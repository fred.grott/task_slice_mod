// BSD Claause 2 Copyright 2020 Fred Grott(Fredrick Allan Grott)


import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:taskslicemod/modules/home/home_module.dart';
import 'package:taskslicemod/modules/root/root_bloc.dart';
import 'package:taskslicemod/modules/root/root_widget.dart';

// No need to specify route transitions for this module as its root
class RootModule extends MainModule {
  @override
  List<Bind> get binds => [
    Bind<dynamic>((i) => RootBloc()),
  ];

  @override
  List<Router> get routers => [
    Router<dynamic>(Modular.initialRoute, module: HomeModule()),
  ];

  @override
  Widget get bootstrap => RootWidget();
}
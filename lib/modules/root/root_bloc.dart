// BSD Claause 2 Copyright 2020 Fred Grott(Fredrick Allan Grott)



import 'dart:async';
import 'dart:ui';

import 'package:flutter_modular/flutter_modular.dart';

class RootBloc extends Disposable {

  Brightness brightness = Brightness.light;

  // this will generate a false linter positive see
  // https://github.com/felangel/bloc/issues/587
  // ignore: close_sinks
  StreamController controller = StreamController<dynamic>();


  @override
  void dispose() {}

}
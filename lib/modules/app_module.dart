// BSD Claause 2 Copyright 2020 Fred Grott(Fredrick Allan Grott)
import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'app_bloc.dart';
import 'app_widget.dart';
import 'home/home_module.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
    Bind<dynamic>((i) => AppBloc()),
  ];

  @override
  List<Router> get routers => [
    Router<dynamic>(Modular.initialRoute, module: HomeModule()),
  ];

  @override
  Widget get bootstrap => AppWidget();
}
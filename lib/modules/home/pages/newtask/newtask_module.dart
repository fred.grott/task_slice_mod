// BSD Claause 2 Copyright 2020 Fred Grott(Fredrick Allan Grott)

import 'package:flutter_modular/flutter_modular.dart';

import 'newtask_bloc.dart';
import 'newtask_widget.dart';

class NewTaskModule extends ChildModule {
  @override
  List<Bind> get binds => [
    Bind<dynamic>((i) => NewTaskBloc()),
  ];

  @override
  List<Router> get routers => [
    Router<dynamic>(
      Modular.initialRoute,
      child: (_, args) => NewTaskWidget(),
    ),
    Router<dynamic>(
      "/newtask/:id",
      child: (_, args) => NewTaskWidget(
        param: int.parse(args.params['id'] as String),
      ),

    ),
  ];

  static Inject get to => Inject<NewTaskModule>.of();
}
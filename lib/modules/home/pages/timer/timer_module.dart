// BSD Claause 2 Copyright 2020 Fred Grott(Fredrick Allan Grott)


import 'package:flutter_modular/flutter_modular.dart';
import 'package:taskslicemod/modules/home/pages/task/task_widget.dart';
import 'package:taskslicemod/modules/home/pages/timer/timer_bloc.dart';
import 'package:taskslicemod/modules/home/pages/timer/timer_widget.dart';

class TimerModule extends ChildModule {
  @override
  List<Bind> get binds => [
    Bind<dynamic>((i) => TimerBloc()),
  ];

  @override
  List<Router> get routers => [
    Router<dynamic>(
      Modular.initialRoute,
      child: (_, args) => TimerWidget(),
    ),
    Router<dynamic>(
      "/task/:id",
      child: (_, args) => TaskWidget(
        param: int.parse(args.params['id'] as String),
      ),

    ),




  ];

  static Inject get to => Inject<TimerModule>.of();
}
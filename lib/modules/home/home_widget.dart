// BSD Claause 2 Copyright 2020 Fred Grott(Fredrick Allan Grott)
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:taskslicemod/modules/home/pages/task/task_widget.dart';

import '../../responsive_utils.dart';
import '../../themes_widgets.dart';
import 'home_module.dart';
// lanniding page
class HomeWidget extends ModularStatelessWidget<HomeModule> {
  HomeWidget({Key key, this.title}) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    ResponsiveConfig.init(context, allowFontScaling: true);

    // need to switch to
    // stack
    //   container
    // box decoration
    // content below appbar
    // positioned widget
    // appBar is child of the positioned widget
    // above this set body to new widget defintion
    // and have new widget def do what I need to do
    return PlatformScaffold(

      iosContentPadding: true,
      appBar: PlatformAppBar(
        title:  PlatformText('Task Slice',),
        //backgroundColor: Colors.transparent,


        android: (_) => myMaterialAppBarData,


        // inmy litview pages need to set transparency
        ios: (_) => myCupertinoNavigationBarData,
        trailingActions: <Widget>[
          PlatformIconButton(
            padding: EdgeInsets.zero,
            iosIcon: Icon(CupertinoIcons.share),
            androidIcon: Icon(Icons.share),

            ios: (_) => myCupertinoIconButtonData,
            android: (_) => myMaterialIconButtonData,
            onPressed: () {},
          ),
        ],
      ),

      //body: HomePage(title: 'Tasks'),



      body: TaskWidget(title: 'Tasks'),

    );
  }
}
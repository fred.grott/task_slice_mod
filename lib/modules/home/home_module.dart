// BSD Claause 2 Copyright 2020 Fred Grott(Fredrick Allan Grott)

import 'package:flutter_modular/flutter_modular.dart';
import 'package:taskslicemod/modules/home/pages/newtask/newtask_widget.dart';
import 'package:taskslicemod/modules/home/pages/timer/timer_widget.dart';


import 'home_bloc.dart';
import 'home_widget.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
    Bind<dynamic>((i) => HomeBloc()),
  ];

  @override
  List<Router> get routers => [
    Router<dynamic>(
      Modular.initialRoute,
      child: (_, args) => HomeWidget(),
    ),
    Router<dynamic>(
      "/newtask/:id",
      child: (_, args) => NewTaskWidget(
        param: int.parse(args.params['id'] as String),
      ),

    ),
    Router<dynamic>(
      "/timer/:id",
      child: (_, args) => TimerWidget(
        param: int.parse(args.params['id'] as String),
      ),
    )


  ];

  static Inject get to => Inject<HomeModule>.of();
}
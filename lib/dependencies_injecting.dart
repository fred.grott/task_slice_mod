import 'package:fimber/fimber_base.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:uuid/uuid.dart';

final Injector myAppInjector = AppModuleContainer().initialise(Injector.getInjector());
// acceptance test modules injection follows the same patttern
// but gets defined in test-driver setup




class AppModuleContainer {
  Injector initialise(Injector injector){


    injector.map<AppBuildVariants>((i) => AppBuildVariants());
    injector.map<AppEndPoint>((i) => AppEndPoint());
    injector.map<AppLogger>((i) => AppLogger(), isSingleton: true);
    injector.map<AppName>((i) => AppName(), isSingleton: true);
    injector.map<AppUuid>((i) => AppUuid(), isSingleton: true);
    injector.map<AppDebugMode>((i) => AppDebugMode(), isSingleton: true);
    injector.map<AppCrashReportAddy>((i) => AppCrashReportAddy(), isSingleton: true);



    return injector;
  }



}

class AppCrashReportAddy{
  final String emailAddy =  getEmailAddy();

  static String getEmailAddy(){
    return 'fred.grott@gmail.com';
  }

}

class AppLogger{
  FimberLog myLogger = getLogger();

  static FimberLog getLogger(){
    return FimberLog("FlutterBoilerplate");
  }

}

class AppUuid{
  final String myAppUuid = getUuid();

  static String getUuid(){
    // time stamped uuid
    return  Uuid().v1();
  }
}
// difference between mobile and web is that web does not
// have a dart vm and thus should use asserts for full cross
// platform compat reasons

// just to show the genric structure of doing strings, vars, etc
class AppName{
  final String myAppName = getAppName();

  static String getAppName(){
    return "FlutterBoilerplate";
  }
}

// General idea is that we want this not initialized until
// that first time in the buildvariants maindelegate block
// and yet be able to initialize the moduel only one time not
// mulitple time or even separate modules, one
// magic place to inject moduels only ie the buildvariant block
class AppBuildVariants{
  //final String endPoint;


  String name(String buildvaraints){
    return buildvaraints;
  }


}

class AppEndPoint {


  String appEndPoint(String endPoint){
    return endPoint;
  }
}

class AppDebugMode {
  final bool isInDebugMode = getDebugMode();

  static bool getDebugMode(){
    bool inDebugMode = false;
    assert(inDebugMode = true);
    return inDebugMode;
  }
}
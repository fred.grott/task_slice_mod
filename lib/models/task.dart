class Task {
   int id;
   String title;
   String description;
  bool done;
  int pomCount;

  Task(this.id, this.title, this.description, this.done, [this.pomCount = 0]);

  factory Task.fromMap(Map<String, dynamic> json) => Task(
      json['id'] as int,
      json['title'] as String,
      json['description'] as String,
      json['done'] == 1 ? true : false,
      json['pomCount'] as int);
}

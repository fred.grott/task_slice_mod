import 'package:taskslicemod/repository/database.dart';
import 'package:taskslicemod/models/task.dart';

import 'database.dart';

class Manager {
  Future<List<Task>> tasksData;

  dynamic addNewTask(Task task) async {
    await DatabaseUtil.db.insert(task);
  }

  dynamic updateTask(Task task) async {
    await DatabaseUtil.db.update(task);
  }

  dynamic removeTask(Task task) async {
    await DatabaseUtil.db.remove(task);
  }

  dynamic loadAllTasks() {
    tasksData = DatabaseUtil.db.getAll();
  }
}

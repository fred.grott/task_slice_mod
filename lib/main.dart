import 'dart:async';

import 'package:catcher/core/catcher.dart';
import 'package:catcher/handlers/console_handler.dart';
import 'package:catcher/handlers/email_manual_handler.dart';
import 'package:catcher/handlers/toast_handler.dart';
import 'package:catcher/mode/dialog_report_mode.dart';
import 'package:catcher/model/catcher_options.dart';
import 'package:catcher/model/toast_handler_gravity.dart';
import 'package:catcher/model/toast_handler_length.dart';
import 'package:fimber/fimber_base.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'dependencies_injecting.dart';
import 'modules/root/root_module.dart';

// Note:
//        I have two di strategies. One is plain simple factory injection for non widget stuff
//        The second is using Modular which uses statefull widgets instead of Google's
//        bone-head inherited widget di as I want zero or close to it singletons so that
//        everyhting is test-able.
//
//        Takes a little more to set up since its tied to bloc and I have the flutter
//        platform widgets stuff to put in but once one gets gooing ts easy.

// All this does now is ste up themes and send app to the root widget on startup
// along with setting a separate zone to keep the app alive in case of
// an app exception



// ignore: prefer_void_to_null
Future<Null> mainDelegate() async{

  // get connection, are we internet connected/

// This captures errors reported by the Flutter framework.
  FlutterError.onError = (FlutterErrorDetails details) async {
    if (myAppInjector.get<AppDebugMode>().isInDebugMode) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // Sentry.
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  if (myAppInjector.get<AppDebugMode>().isInDebugMode) {

  } else {
    // not in docs but in the fimber class itself at
    // https://github.com/magillus/flutter-fimber/blob/master/fimber/lib/fimber.dart
    final List<String> myLogLevels = ["D", "I", "W", "E"];
    Fimber.plantTree(DebugTree(useColors: true, logLevels:  myLogLevels, printTimeType: 1));
  }
  // per this so answer https://stackoverflow.com/questions/57879455/flutter-catching-all-unhandled-exceptions
  await runZoned<Future<void>>(
          () async {

        final CatcherOptions debugOptions =
        CatcherOptions(DialogReportMode(), [ConsoleHandler()]);
        final CatcherOptions releaseOptions = CatcherOptions(DialogReportMode(), [
          EmailManualHandler([myAppInjector.get<AppCrashReportAddy>().emailAddy])
        ]);
        CatcherOptions(DialogReportMode(), [
          // not needed as can integrae with sentry.io
          //EmailManualHandler([myAppInjector.get<AppCrashReportAddy>().emailAddy],
              //enableDeviceParameters: true,
              //enableStackTrace: true,
              //enableCustomParameters: true,
              //enableApplicationParameters: true,
              //sendHtml: true,
             // emailTitle: (myAppInjector.get<AppName>().myAppName) + " Exceptions",
              //emailHeader: "Exceptions",
             // printLogs: true)
        ]);
        CatcherOptions(DialogReportMode(), [
          ToastHandler(
              gravity: ToastHandlerGravity.bottom,
              length: ToastHandlerLength.long,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              textSize: 12.0,
              customMessage: "We are sorry but unexpected error occured.")
        ]);
        CatcherOptions(DialogReportMode(), [ConsoleHandler(enableApplicationParameters: true,
            enableDeviceParameters: true,
            enableCustomParameters: true,
            enableStackTrace: true)]);


        Catcher(Main(), debugConfig: debugOptions, releaseConfig: releaseOptions);


      }, onError: (dynamic error, dynamic stackTrace) {
    Catcher.reportCheckedError(error, stackTrace);
  });

}

class Main extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) => ModularApp(module: RootModule());
}

